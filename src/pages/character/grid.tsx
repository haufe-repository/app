import { useState, useEffect } from 'react'
import { Loading } from '@components/ui'
import { CharacterService } from '@services/index'
import { CharacterCard } from '@scenes/index'
import { CharacterProps } from '@utils/types'

const CharacterGrid = () => {

    const [loading, setLoading] = useState<boolean>(true)
    const [characterGrid, setCharacterGrid] = useState<Array<CharacterProps> | undefined>()

    useEffect(() => {
        let active: boolean = true;
        (async () => {
            const newData = await CharacterService.list()
            if (!active) {
                return
            }
            setLoading(false)
            setCharacterGrid(newData.results)
        })()
        return () => { active = false }
    }, [setCharacterGrid])

    return (
        <section>
            {!loading ? (
                characterGrid && characterGrid.length > 0 ? (
                    <div className="ds--showcase">
                        {characterGrid.map((character, index) => (
                            <CharacterCard 
                                key={index}
                                {...character}  
                            />
                        ))}
                    </div>
                ) : (
                    <p>No results found.</p>
                )
            ) : (
                <Loading/>
            )}
        </section>
    )
}

export default CharacterGrid
