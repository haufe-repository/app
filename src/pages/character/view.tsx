import { useState, useEffect } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { Loading } from '@components/ui'
import { CharacterService } from '@services/index'
import { CharacterProps } from '@utils/types'
import { CharacterCard } from '@scenes/index'
import { NotFound } from '@pages/index'

type Params = { id: string }

interface CharacterDetailProps extends RouteComponentProps<Params> {}

const CharacterDetail = ({ match }: CharacterDetailProps) => {

    const [characterView, setSharacterView] = useState<CharacterProps | undefined>()
    const [error, setError] = useState<boolean>(false)

    useEffect(() => {
        let active: boolean = true;
        (async () => {

            setError(false)
            setSharacterView(undefined)

            if (!active) {
                return
            }

            try {
                const request = await CharacterService.detail(match.params.id)
                if (request) {
                    setSharacterView(request)
                }
            } catch (error: any) {
                setError(true)
            }

        })()
        return () => { active = false }
    }, [match, setSharacterView])

    return (
        <section>
            {characterView ? (
                <div className="ds--showcase">
                    <CharacterCard {...characterView} />
                </div>
            ) : (
                !error ? <Loading /> : <NotFound />
            )}
        </section>
    )
}

export default CharacterDetail
