import { createStore, combineReducers, applyMiddleware } from 'redux'
import ReduxThunk from 'redux-thunk'

import { sessionReducer } from './session/reducer'
import { SessionActionTypes } from './session/action'
import { SessionProps, CharacterProps } from '@utils/types'

export interface AppState {
    session: SessionProps,
    characterGrid: Array<CharacterProps>,
    characterView: CharacterProps
}

export {
    SessionActionTypes
}

const reducers = combineReducers({
    session: sessionReducer
})

const store = createStore(reducers, applyMiddleware(ReduxThunk))

export default store
