export enum SessionActionTypes {
    SAVE = 'SAVE',
    REMOVE = 'REMOVE',
}

export interface SessionAction {
    type: SessionActionTypes.SAVE | SessionActionTypes.REMOVE
    payload: any
}

export const save: SessionAction = {
    type: SessionActionTypes.SAVE,
    payload: {},
}

export const remove: any = {
    type: SessionActionTypes.REMOVE,
}
