import { SessionAction, SessionActionTypes } from './action'
import { removeToken } from '@utils/utilities'

const token = localStorage.getItem('haufe_api_token')
const initialSessionProps = token ? { token: token } : {}

export const sessionReducer = (
  state: any = initialSessionProps,
  action: SessionAction
): any => {
  let newState = Object.assign({}, state)
  switch (action.type) {
    case SessionActionTypes.SAVE:
      newState = {
        ...state,
        ...action.payload,
      }
      return newState
    case SessionActionTypes.REMOVE:
      removeToken()
      newState = {}
      return newState
    default:
      return state
  }
}
