import request from '@utils/request'

function list(page: number | string = 1, query?: any) {
    return request({
        url: `character/list/${page}`,
        method: 'GET',
        params: query
    })
}

function detail(id: number | string) {
    return request({
        url: `character/detail/${id}`,
        method: 'GET'
    })
}

const CharacterService = {
    list, detail
}

export default CharacterService
