import request from '@utils/request'

function save(id: number | string) {
    return request({
        url: `favorite/save`,
        method: 'POST',
        data: { id }
    })
}

function remove(id: number | string) {
    return request({
        url: `favorite/remove`,
        method: 'DELETE',
        data: { id }
    })
}

const FavoriteService = {
    save, remove
}

export default FavoriteService
