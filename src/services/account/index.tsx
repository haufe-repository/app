import request from '@utils/request'
import { SignInProps } from '@utils/types'

function signIn(params: SignInProps) {
    return request({
        url: `account/sign-in`,
        method: 'POST',
        data: params
    })
}

const AccountService = {
    signIn
}

export default AccountService
