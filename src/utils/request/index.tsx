import axios from 'axios'
import DEFAULT from '@config/default'

const client = axios.create({
    baseURL: DEFAULT.endpoint.api
})

client.interceptors.request.use((config) => { 
    const token = localStorage.getItem('haufe_api_token')
    if(token){
        config.headers = {
            Authorization: token,
            ...config.headers
        }   
    } 
    return config
}, (error) => { 
  Promise.reject(error)
})

const request = function (options: any) {
    const onSuccess = function (response: any) {
        return response.data
    }

    const onError = function (error: any) {
        return Promise.reject(error.response || error.message)
    }

    return client(options)
        .then(onSuccess)
        .catch(onError)
}

export default request
