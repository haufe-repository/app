export type CharacterProps = {
    id: number,
    name: string
    status: 'Alive' | 'Dead' | 'unknown'
    species: string
    type: string,
    gender: 'Female' | 'Male' | 'Genderless' | 'unknown',
    origin: OriginProps,
    location: LocationProps,
    image: string,
    episode: Array<string>
    url: string,
    created: string,
    favorite?: FavoriteProps
}

export type LocationProps = {
    id: number,
    name: string
    type: string,
    dimension: string,
    residents: Array<string>,
    url: string,
    created: string
}

export type OriginProps = {
    name: string
    url: string
}

export type FavoriteProps = {
    _id: string,
    reference_id: number
}
