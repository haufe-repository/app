export type SignInProps = {
    email: string
    password: string
}

export type SessionProps = {
    _id: number,
    name: string,
    surname: string,
    email: string,
    created_at?: string,
    updated_at?: string,
    token?: string
}
