const setToken = (token: string) => {
    localStorage.setItem('haufe_api_token', token)
}

const removeToken = () => {
    localStorage.removeItem('haufe_api_token')
}

export { setToken, removeToken }
  