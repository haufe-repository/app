import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Form, Field } from 'react-final-form'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { Button } from '@components/ui'
import { AccountService } from '@services/index'
import { SignInProps, SessionProps } from '@utils/types'
import { SessionActionTypes } from '@store/index'
import { setToken } from '@utils/utilities'

interface FormSignInProps {
    saveSesion(payload: SessionProps): any
}

const FormSignIn = ({ saveSesion }: FormSignInProps) => {

    const history = useHistory()
    const [submitting, setSubmitting] = useState<boolean>(false)

    const onSubmit = async (values: SignInProps) => {
        
        setSubmitting(true)

        try {
            
            const signIn = await AccountService.signIn(values) as SessionProps

            if (signIn.token) {

                // Save sesion on redux store 
                saveSesion(signIn)
    
                // Save token on localStorage
                setToken(signIn.token) 
    
                // Redirect to main page
                history.push('/characters')
    
            } 

        } catch (err: any) {
            
            setSubmitting(false)

            alert(err.data.message)

        }

    }

    return (
        <div className="ds--form__sign-in">
            <Form
                onSubmit={onSubmit}
                render={({ handleSubmit }) => (
                    <form onSubmit={handleSubmit}>
                        <Field name="email">
                            {({ input, meta }) => (
                                <div className="form-group">
                                    <label>Email</label>
                                    <input {...input} type="text" className="form-control"/>
                                    {meta.error && meta.touched && <span>{meta.error}</span>}
                                </div>
                            )}
                        </Field>
                        <Field name="password">
                            {({ input, meta }) => (
                                <div className="form-group">
                                    <label>Password</label>
                                    <input {...input} type="password" className="form-control"/>
                                    {meta.error && meta.touched && <span>{meta.error}</span>}
                                </div>
                            )}
                        </Field>
                        <div>
                            <Button type="submit" fullWidth disabled={submitting}>
                                Submit
                            </Button>
                        </div>
                    </form>
                )}
            />
        </div>
    )
}

const mapStateToProps = () => ({})

const mapDispatchToProps = (dispatch: Dispatch) => ({
    saveSesion: (payload: SessionProps) => {
        dispatch({
            type: SessionActionTypes.SAVE,
            payload: payload,
        })
    }
})

export default   connect(mapStateToProps, mapDispatchToProps)(FormSignIn)
