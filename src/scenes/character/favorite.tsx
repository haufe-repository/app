import { useState } from 'react'
import { Button } from '@components/ui'
import { CharacterProps, FavoriteProps } from '@utils/types'
import { FavoriteService } from '@services/index'

const ButtonFavoriteCharacter = ({ id, favorite }: CharacterProps) => {

    const [isFavorite, setIsFavorite] = useState<boolean>(favorite ? true : false)
    const [submitting, setSubmitting] = useState<boolean>(false)

    const handleSave = async (id: number) => {
        setSubmitting(true)
        FavoriteService.save(id).then((response: FavoriteProps) => {
            setSubmitting(false)
            setIsFavorite(true)
        }).catch((error: any) => {

        })
    }

    const handleRemove = async (id: number) => {
        setSubmitting(true)
        FavoriteService.remove(id).then((response: FavoriteProps) => {
            setSubmitting(false)
            setIsFavorite(false)
        }).catch((error: any) => {
            
        })
    }

    return (
        !isFavorite ? (
            <Button disabled={submitting} onClick={() => handleSave(id)}>
                Save favorite
            </Button>
        ) : (
            <Button disabled={submitting} onClick={() => handleRemove(id)}>
                Remove favorite
            </Button>
        )
    )
}

export default ButtonFavoriteCharacter
