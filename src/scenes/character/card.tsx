import { Link } from 'react-router-dom'
import { CharacterProps } from '@utils/types'
import { ButtonFavoriteCharacter } from '@scenes/index'

const CharacterCard = (props: CharacterProps) => {
    return(
        <article className="ds--character__card">
            <div className="ds--character__card-imageWrapper">
                <img src={props.image} alt={props.name} className="ds--character__card-image"/>
            </div>
            <div className="ds--character__card-contentWrapper">
                <div className="ds--character__card-header">
                    <Link to={`/character/view/${ props.id }`} title={props.name}>
                        <h2 className="ds--character__card-title">
                            {props.name}
                        </h2>
                    </Link>
                </div>
                <ButtonFavoriteCharacter {...props}/>
            </div>
        </article>
    )
}

export default CharacterCard
