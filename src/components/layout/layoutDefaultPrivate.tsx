import React from 'react'
import { Route, RouteProps } from 'react-router-dom'
import { Header } from '@components/shared'

interface LayoutProps {
    children: React.ReactNode
}

const Layout = ({ children }: LayoutProps) => {
    return (
        <div className="ds--layout_private">
            <Header/>
            <main className="ds--main">
                {children}
            </main>
        </div>
    )
}

interface LayoutDefaultPrivateProps extends RouteProps {
    component: any
}

const LayoutDefaultPrivate = ({ component: Component, ...rest }: LayoutDefaultPrivateProps) => {
    return (
        <Route {...rest} render={matchProps => (
            <Layout>
                <Component {...matchProps} />
            </Layout>
        )} />
    )
}

export default LayoutDefaultPrivate
