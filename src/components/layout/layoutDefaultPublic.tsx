import React from 'react'
import { Route, RouteProps } from 'react-router-dom'

interface LayoutProps {
    children: React.ReactNode
}

const Layout = ({ children }: LayoutProps) => {
    return (
        <div className="ds--layout_public">
            <main className="ds--main">
                {children}
            </main>
        </div>
    )
}

interface layoutDefaultPublicProps extends RouteProps {
    component: any
}

const layoutDefaultPublic = ({ component: Component, ...rest }: layoutDefaultPublicProps) => {
    return (
        <Route {...rest} render={matchProps => (
            <Layout>
                <Component {...matchProps} />
            </Layout>
        )} />
    )
}

export default layoutDefaultPublic
