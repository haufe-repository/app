import React from 'react'

interface LoadingProps {
    placeholder?: string
}

const Loading = ({ placeholder = 'Loading...' }: LoadingProps) => {
    return (
        <div className="loading">
            { placeholder }
        </div>
    )
}

export default Loading

