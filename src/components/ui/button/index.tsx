import React from 'react'
import classNames from 'classnames'

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    children: React.ReactNode,
    fullWidth?: boolean,
    color?: 'primary' | 'secondary'
}

const Button = ({ children, color = 'primary', fullWidth = false, ...rest }: ButtonProps) => {
    return (
        <button className={
            classNames(
                'btn', 
                `btn-${ color }`, 
                fullWidth ? 'btn-fullWidth' : undefined
            )
        } {...rest}>
            { children }
        </button>
    )
}

export default Button

