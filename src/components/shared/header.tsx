import React from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { Button } from '@components/ui'
import { SessionActionTypes } from '@store/index'

interface HeaderProps {
    signOut: () => void
}

const Header = ({ signOut }: HeaderProps) => {

    const handleSignOut = () => {
        signOut()
    }

    return (
        <header className="ds--header">
            <div className="ds--header__left">
                <h1>The Rick and Morty App</h1>
            </div>
            <div className="ds--header__right">
                <Button onClick={handleSignOut}>Sign out</Button>
            </div>
        </header>
    )
}

const mapStateToProps = () => ({})

const mapDispatchToProps = (dispatch: Dispatch) => ({
    signOut: () => {
        dispatch({
            type: SessionActionTypes.REMOVE
        })
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Header)
