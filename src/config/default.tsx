const DEFAULT = {
    endpoint: {
        api: process.env.REACT_APP_API_HOST
    }
}

export default DEFAULT
