import { Route, Switch, HashRouter, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { LayoutDefaultPrivate, LayoutDefaultPublic } from '@components/layout'
import { SignIn, CharacterGrid, CharacterView } from '@pages/index'
import { AppState } from '@store/index'
import { SessionProps } from '@utils/types'

import '@assets/scss/index.scss'

interface AppProps {
  session: SessionProps
}

const App = ({ session }: AppProps) => {
  return (
    <div className="ds--app">
          {session.token ? (
            <HashRouter>
              <Switch>
                <Route exact path="/">
                  <Redirect to="/characters" />
                </Route>
                <LayoutDefaultPrivate exact path="/characters" component={CharacterGrid} />
                <LayoutDefaultPrivate exact path="/characters/page/:page" component={CharacterGrid} />
                <LayoutDefaultPrivate exact path="/character/view/:id" component={CharacterView} />
                <Redirect to="/" />
              </Switch>
            </HashRouter>
          ) : (
            <HashRouter>
              <Switch>
                <Route exact path="/">
                  <Redirect to="/sign-in" />
                </Route>
                <LayoutDefaultPublic exact path="/sign-in" component={SignIn} />
                <Redirect to="/" />
              </Switch>
            </HashRouter>
          )}
    </div>
  )
}

const mapStateToProps = ({ session }: AppState) => ({
  session
})

export default connect(mapStateToProps)(App)
