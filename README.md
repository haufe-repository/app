## Requirements

1. Node >= 14
2. NPM >= 6
3. Yarn

```
npm install --global yarn
```

## Instructions

1. clone repo

```
git clone git@gitlab.com:haufe-repository/app.git
```

2. cd into directory

```
cd app
```

3. install dependencies using npm or yarn

```
npm install
```

Inside the project folder, you can run some built-in commands:

### `npm run start`

Runs the app in development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
